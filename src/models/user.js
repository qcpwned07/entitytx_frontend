const Entity = require('./entity');
const Tx = require('./tx');


class User extends Entity {
    constructor(attr) {
        // Validate Attributes
        if (!attr.email)
            throw "Please enter a valid email";
        else if (attr.email.validate)
            attr.email.validate(attr.email.value)
        else if (!attr.password)
            throw "Field \'Password\' is required";
        else if (!attr.name)
            throw "Field \'Name\' is required";
        else if (!attr.location)
            throw "Problem while validating the address";
        else if (!attr.pictureURI)
            throw "Field \'Picture\' is required";
        super('Entitys', 'User', attr.email, attr)
    }


    chgUsername(source, value){
        maTransaction = new UserTx(source, "username", (x)=>{return value});

    }


    newTransaction() {
        return new UserTx();
    }

}

class UserTx extends Tx {

    constructor(){
        super('Txs', 'UserTx');

    }
}

/*

Code for testing purposes
TODO: Remove once tested and working


// Declare a transaction which is a tuple with the property name and the function
let tx = {propertyName: "spin", transaction: ()=>{return 'righty'}}
// Create entity
//let entity = new Entity('atom', 'Entity', {spin: 'lefty'},);
// Create a transaction

// Create User attributes
let attr = {
    email: "qcpwned07@gmail.com",
    password: "password",
    name: "Matthieu Picard",
    location: "No location yet",
    pictureURI: "./pictures/me",
}

//Create the user
let matthieu = new User(attr);
console.log(matthieu);

let transaction = (old)=>{return "F"};
let userTransaction = new Tx('UserTx', 'Change Username', 'Bob', 'Matthieu', "email", transaction);

//Execute the transaction on the entity
matthieu.doTx(userTransaction)
console.log(matthieu);

console.log(userTransaction);
*/



module.exports = User;