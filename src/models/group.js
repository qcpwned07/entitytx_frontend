
class Group {

    constructor(
        {
            _id,
            name,
            description,
            img,
            type,
            values,

            members         = [],
            creator,

            // Tree structure
            children        = [],
            parent,
            active          = true,
            attr            = [],

            // Transactionnal
            _txIn           = [],
            _txOut          = [],

            //Metadata
            timestamp = Date.now(),

            // If we want to create from JSON object
            // TODO: implement
            jsonEntity = {},
        }
    ) {
        this._id            = _id;
        this.name           = name;
        this.description    = description;
        this.img            = img;
        this.type           = type;
        this.values         = values;

        this.members        = members;
        this.creator        = creator;

        this.children       = children;
        this.parent         = parent;

        this.active         = active;
        this.timestamp      = timestamp;
        this._txIn          = _txIn;
        this._txOut         = _txOut;

        // TODO: Not used for now, probably will be useful
        //this.setProperties = (props) => {this.properties = props};
        //this.doTx = (tx) => {this._txIn.push(tx)};
        //this.makeTx = (tx) => {this._txOut.push(tx)};

        // Freezing the object to make it immutable
        //Object.freeze(this);

        console.log(typeof _txIn + " created.");
    }


    // Old code may not be usefull
    doTx(tx) {
        this._txIn.push(tx);
        console.log(this);
        let property = this.properties[tx.target];
        this.properties[tx.target] = tx.transaction(property);

    }


    get id() {return this._id}

    get txIn() {return this._txIn}
    set txIn(x) {
        if (typeof x != 'Tx')
            throw "txIn has to be a transaction";
        this._txIn.push(x);
    }

    get txOut() {return this._txOut}
    set txOut(x) {
        if (typeof x != 'Tx')
            throw "txOut has to be a transaction";
        this._txOut.push(x);
    }


}

module.exports = {Group};

