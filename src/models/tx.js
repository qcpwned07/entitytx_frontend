
class Tx {

    constructor(
        {
            _id,
            name,
            description,
            values,
            validators,
            group,

            // Tree structure
            parent,
            children        = [],

            // Metadata
            type            = 'Tx',
            active          = true,
            issuer,
            acquirer,
            timestamp       = Date.now(),

            // If we want to create from JSON object
            // TODO: implement
            jsonTx = {},
        }
    ) {
        this._id            = _id;
        this.name           = name;
        this.description    = description;
        this.values         = values;
        this.validators     = validators;
        this.group          = group;

        this.parent         = parent;
        this.children       = children;

        this.type           = type;
        this.active         = active;
        this.issuer         = issuer;
        this.acquirer       = acquirer;
        this.timestamp      = timestamp;

        // TODO: Not used for now, probably will be useful
        //this.setProperties = (props) => {this.properties = props};
        //this.doTx = (tx) => {this._txIn.push(tx)};
        //this.makeTx = (tx) => {this._txOut.push(tx)};

        // Freezing the object to make it immutable
        //Object.freeze(this);

        console.log(typeof this + " created.");
    }


    // GETTERS
    // and
    // SETTERS
    //
    get id() {return this._id}

    get children() {return this.children}
    set children(x) {
        if (typeof x != 'Tx')
            throw "txIn has to be a transaction";
        this.children.push(x);
    }

    get parent() {return this.parent}
    set parent(x) {
        if (typeof x != 'Tx')
            throw "txIn has to be a transaction";
        this.parent = x;
    }


}

module.exports = Tx;

