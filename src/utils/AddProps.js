import React from "react";

export default function addProps(children, props) {
    let propsNoChildren = Object.assign({}, props);

    // Remove the children if was passed
    if(props.children)
        delete propsNoChildren.children;

    return React.Children
        .map(children, child => {
            return React.cloneElement(child, propsNoChildren);
        });
}