

function capitalizeFirst(string) {
    //string[0] = string.charAt(0).toUpperCase()
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function hasChildren(entity){
    return entity.children.length > 0;
}

export {capitalizeFirst, hasChildren};