"use strict";

import HttpService from './HttpService';
import axios from 'axios';

export default class Http {

    static baseURL(model) {
        if (model === "events")
            return "/api/replacement";
        else
            return "/api/group/g"
    }

    static struct(model){
        return new Promise((resolve, reject) => {

        axios.get(`${this.baseURL(model)}/${model}/struct`).then(response => {
                console.log("Struct")
                console.log(response)
                resolve(response.data);
            }).catch(error => {
                reject(error);
            });
        });
            /*
              HttpService.get(`${this.baseURL(model)}/${model}/struct`, function(data) {
              }, function(textStatus) {
                  reject(textStatus);
              });
              */
        }

        /*
            Function returning a list of
         */
    static search(model, params){
        console.log("Searching : ")
        return new Promise((resolve, reject) => {
        axios.post(`${this.baseURL(model)}/${model}/search`, params).then(response => {
                console.log(response)
                resolve(response.data);
            }).catch(error => {
                reject(error);
            });
        });
    }

    static list(model){
        console.log("LISTING : ")
        return new Promise((resolve, reject) => {
            axios.get(`${this.baseURL(model)}/${model}`).then(response => {
                console.log("Response");
                console.log(response.data)
                resolve(response.data);
            }).catch(error => {
                console.log("Error");
                console.log(error);
                reject(error);
                console.log(error)
            });
        });
    }

    static get(model, id) {
        return new Promise((resolve, reject) => {
            axios.get(`${this.baseURL(model)}/${model}/${id}`).then(response => {
                if(response.data != undefined || Object.keys(response.data).length !== 0) {
                    resolve(response.data);
                }
                else {
                    reject('Error while retrieving entity');
                }
            }).catch(error => {
                reject(error);
            });
        });
    }

    static delete(model, id) {
        return new Promise((resolve, reject) => {
            HttpService.remove(`${Http.baseURL(model)}/${model}/${id}`, function(data) {
                if(data.message != undefined) {
                    resolve(data.message);
                }
                else {
                    reject('Error while deleting');
                }
            }, function(textStatus) {
                reject(textStatus);
            });
        });
    }

    static put(model, entity) {
        return new Promise((resolve, reject) => {
            HttpService.put(`${this.baseURL(model)}/${model}/${entity._id}`, entity, function(data) {
                resolve(data);
            }, function(textStatus) {
                console.log(textStatus);
                reject(textStatus);
            });
        });
    }

    static post(model, entity) {
        entity.id = Math.floor((Math.random() * 100000000) + 1).toString();
        return new Promise((resolve, reject) => {
            HttpService.post(`${this.baseURL(model)}/${model}`, entity, function(data) {
                resolve(data);
            }, function(textStatus) {
                console.log(textStatus);
                reject(textStatus);
            });
        });
    }

    /*
    Returns a list of entity (of type: model) with the fields specified populated with data

    @param model (string): Name of the model you want to work on
    @param fields [string]: List of names of field you want populated

    @return [Entity} : List of the desired type with its desired fields populated
     */
    static populatedList(model, fields) {
        return new Promise((resolve, reject) => {
            axios.post(`${this.baseURL(model)}/${model}/populated/fields`, {fields: fields}).then(response => {
                resolve(response.data);
            }).catch(error => {
                reject(error);
            });
        });
    }
}
