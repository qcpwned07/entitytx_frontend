import HttpService from './HttpService';
//import axios from 'axios';

export default class Http {

    static baseURL() {return "http://localhost:7560/api/group/g"}

    static struct(model){
        return new Promise((resolve, reject) => {
            HttpService.get(`${this.baseURL()}/${model}/struct`, function(data) {
                console.log(data);
                resolve(data);
            }, function(textStatus) {
                reject(`Unable to get ${model}'s model :\n${textStatus}`);
            });

            /*
        axios.get(`${this.baseURL()}/${model}/struct`).then(response => {
                resolve(response.data);
            }).catch(error => {
                reject(error);
            });

             */
        });
    }

    static search(model, params){
        return new Promise((resolve, reject) => {
            HttpService.get(`${this.baseURL()}/${model.name}`, function(data) {
                resolve(data.map(entity => Http.buildEntity(model, entity)));
            }, function(textStatus) {
                console.log(textStatus);
                reject(textStatus);
            });
            /*
        axios.post(`${this.baseURL()}/${model.name}/search`).then(response => {
                const entities = response.data.map(entity => this.buildEntity(model, entity));
                resolve(entities);
            }).catch(error => {
                reject(error);
            });
             */
        });
    }

    static list(model){
        return new Promise((resolve, reject) => {
            HttpService.get(`${this.baseURL()}/${model.name}`, function(data) {
                console.log(data);
                var entities = data.map(entity => Http.buildEntity(model, entity));
                console.log(entities);
                resolve(entities);
            }, function(textStatus) {
                reject(textStatus);
            });
            /*
            axios.get(`${this.baseURL()}/${model.name}`).then(response => {
                const entities = response.data.map(entity => this.buildEntity(model, entity));
                resolve(entities);
            }).catch(error => {
                reject(error);
            });
             */
        });
    }

    static get(model, id) {
        return new Promise((resolve, reject) => {
            HttpService.get(`${this.baseURL()}/${model}/${id}`, function(data) {
                resolve(Http.buildEntity(model, data));
            }, function(textStatus) {
                console.log(textStatus);
                reject(textStatus);
            });
          /*
            axios.get(`${this.baseURL()}/${model.name}/${id}`).then(response => {
                if(response.data != undefined || Object.keys(response.data).length !== 0) {
                    resolve(this.buildEntity(model, response.data));
                }
                else {
                    reject('Error while retrieving entity');
                }
            }).catch(error => {
                reject(error);
            });
            */
        });
    }

    static delete(model, id) {
        return new Promise((resolve, reject) => {
            HttpService.remove(`${Http.baseURL()}/${model.name}/${id}`, function(data) {
                if(data.message !== undefined) {
                    resolve(data.message);
                }
                else {
                    reject('Error while deleting');
                }
            }, function(textStatus) {
                reject(textStatus);
            });
        });
    }

    static put(model, entity) {
        return new Promise((resolve, reject) => {
            HttpService.put(`${this.baseURL()}/${model}/${entity._id}`, entity, function(data) {
                resolve(data);
            }, function(textStatus) {
                console.log(textStatus);
                reject(textStatus);
            });
        });
    }

    static post(model, entity) {
        entity.id = Math.floor((Math.random() * 100000000) + 1).toString();
        return new Promise((resolve, reject) => {
            HttpService.post(`${this.baseURL()}/${model}`, entity, function(data) {
                resolve(data);
            }, function(textStatus) {
                console.log(textStatus);
                reject(textStatus);
            });
        });
    }

    static buildEntity(model, entity) {
       // Put the values into the properties
      let processedEntity ={};
       entity.values.forEach(value => {
            processedEntity[value.name] = value.value;
       });

       processedEntity.id = entity._id;
       processedEntity.attr = this.populateAttr(model.attr, entity.values);
        model.attr.forEach(attr => {
            // If primary, create field and put it
            var val;
            if (attr.primary) {
                // Find the value related to the attribute
                val = entity.values.find(val => val && val.name === attr.name);
                // If one found put the valu in primary
                processedEntity.primary = val ? val.value : null;
            } else if (attr.secondary) {
                // Find the value related to the attribute
                val = entity.values.find(val => val && val.name === attr.name);
                // If one found put the valu in primary
                processedEntity.primary = val ? val.value : null;
            }
        });
       return processedEntity;
    }

    static populateAttr (attr, values) {

        return attr.map(attr=> {
            // Create a copy of the object
            attr = {...attr};
            attr.value = values.find(val => val.name === attr.name);
            attr.value = attr.value ? attr.value.value : null;
            return attr;
        });


    }

}
