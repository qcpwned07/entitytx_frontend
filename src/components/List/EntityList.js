
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import EntityListItem from "./EntityListItem";
import Http from "../../http/GenericService_special";

const styles = theme => ({
    root: {
        width: '100%',
        maxWidth: 460,
        position: 'relative',
        overflow: 'auto',
        maxHeight: 300,
    },
    inline: {
        display: 'inline',
    },
});

class EntityList extends  React.Component {

    constructor(props) {
        super(props);
        this.state = {
            selectedIndex: 0,
            isLoading: true,
            entities: this.props.entities,
            model: this.props.model
        };

        this.handleListItemClick = this.handleListItemClick.bind(this);
    }

    componentDidMount(){

        // If no List was provided, load one
        if (!this.props.entities)
            Http.list(this.props.model).then( response => {
                this.setState({entities: response, isLoading: false});
            })
        else
            this.setState({isLoading:false});
    }

    /*
    To be executed on onclick.
       Change selected item and execute props.onClick
       @param event;
       @param index: new index
     */
    handleListItemClick = (event, index) => {
        this.setState({selectedIndex:index});
        if(this.props.onClick)
            this.props.onClick();
    };


    render() {
        var classes = this.props.classes;
        var indexes = 1;

        return (
            <List className={classes.root}>
                {this.props.list.map(entity =>
                    <EntityListItem
                        attr={ this.props.attr}
                        onClick={this.props.onClick}
                        entity={entity}
                    />
                )}
                {/*
                <ListItem
                        alignItems="flex-start"
                        selected={this.state.selectedIndex === indexes}
                        onClick={event => handleListItemClick(event, indexes++, entity)}
                >
                    <ListItemAvatar>
                        <Avatar
                            alt={entity.name}
                            src={
                                entity.img ?
                                    entity.img :
                                    "https://francophonie-en-fete.com/wp-content/uploads/2019/06/Louis-Jos%C3%A9-Houde-2019-copie-e1561481185366-600x614.jpg"
                            }
                                />
                                </ListItemAvatar>
                    <ListItemText
                        primary="Brunch this weekend?"
                        secondary={
                            <React.Fragment>
                                <Typography
                                    component="span"
                                    variant="body2"
                                    className={classes.inline}
                                    color="textPrimary"
                                >
                                    {entity.email}
                                </Typography>
                                {" — I'll be in your neighborhood doing errands this…"}
                            </React.Fragment>
                        }
                    />
                </ListItem>
                )}
                */}

                <Divider variant="inset" component="li"/>

            </List>
        );
    }
};


export default withStyles(styles)(EntityList);