import React from 'react';
import axios from 'axios';
import { FormattedMessage } from 'react-intl';
import defaultStrings from '../../../i18n/en.json';
import FullDialog from '../../../components/utils/FullDialog.js';
//import CompanyCoreForm from '../../../components/Company/CompanyCoreForm/CompanyCoreForm';
import UserForm from '../../entitytx/components/form/GenericForm.js';
import EntitiesView from './EntitiesView';
import Http from '../../http/GenericService_special.js';
import GenericForm from "../../entitytx/components/form/GenericForm";

class Entities extends React.Component {
  state = {
    searchOptions: {},
    entities: null,
    entity: null,
    createEntityOpen: false,
    editEntityOpen: false,
    editEntityCoreOpen: false,
    isLoading: true,

    model: this.props.model,
  };

  componentDidMount() {
    //this.retrieveEntitySearchOptions();
    this.retrieveEntityStructure();
  }

  retrieveEntityStructure = () => {
      // TODO CHANGE FOR APPROPRIATE
      Http.struct(this.state.model).then(response => {
      this.setState({ entity: response });
      this.searchCompanies();
    });
  }

  retrieveEntitySearchOptions = () => {
    const { groups: searchOptions } = this.state.searchOptions;
    const self = this;

    // TODO CHANGE FOR APPROPRIATE
    //axios.post(`/api/group/g/${this.state.model}/search/options`, { searchOptions }).then(response => {
    //  self.setState({ searchOptions: response.data.searchOptions });
    //  self.searchCompanies();
    //});
      self.searchCompanies();
  }

  searchCompanies = () => {
    const { searchOptions } = this.state;
    const self = this;


    Http.list(this.state.model ).then(response => {
      self.setState({ entities: response , isLoading:false});
    }).catch(response =>{
      console.log(response)
    });
  }

  deleteCompany = entity => {
    Http.delete(`${this.state.model}`,  entity._id);
  }

  changeSearchParameters = event => {
    const { name, value } = event.target;
    let searchOptions = { ...this.state.searchOptions };
    searchOptions[name] = value;
    this.setState({ searchOptions }, this.searchCompanies);
  };

  createEntity = () => {
    this.setState({ createEntityOpen: true });
  }

  createEntityCallback = () => {
    this.searchCompanies();
    this.closeCreateEntity();
  }

  closeCreateEntity = () => {
    this.setState({ createEntityOpen: false });
  }

  editCompany = entity => {
    this.setState({
      editEntityOpen: true,
      selectedEntity: entity,
    });
  }

  editCompanyCallback = () => {
    this.searchCompanies();
    this.closeEditCompany();
  }

  closeEditCompany = () => {
    this.setState({ editEntityOpen: false });
  }

  editCompanyCore = entity => {
    this.setState({
      editEntityCoreOpen: true,
      selectedEntity: entity,
    });
  }

  editCompanyCoreCallback = () => {
    this.searchCompanies();
    this.closeEditCompanyCore();
  }

  closeEditCompanyCore = () => {
    this.setState({ editEntityCoreOpen: false });
  }

  render() {
    const { entities, searchOptions, createEntityOpen, editEntityOpen, editEntityCoreOpen, selectedEntity } = this.state;

    return (
      <React.Fragment>
        <FullDialog
          title={<FormattedMessage {...defaultStrings.commonSentence.common2} />}
          open={createEntityOpen}
          onClose={this.closeCreateEntity}
        >
            {//<CompanyCoreForm
            }
            <GenericForm
            model={this.state.model}
            callback={this.createEntityCallback}
          />
        </FullDialog>
        <FullDialog
          title={<FormattedMessage {...defaultStrings.commonSentence.common7} />}
          open={editEntityOpen}
          onClose={this.closeEditCompany}
        >
          <GenericForm
            model = {this.state.model}
            id = {selectedEntity && selectedEntity._id}
            callback={this.editCompanyCallback}
          />
        </FullDialog>
        <FullDialog
          title={<FormattedMessage {...defaultStrings.commonSentence.common7} />}
          open={editEntityCoreOpen}
          onClose={this.closeEditCompanyCore}
        >
            {/*<CompanyCoreForm*/}
            <GenericForm
            id = {selectedEntity && selectedEntity._id}
            model = {this.state.model}
            callback={this.editCompanyCoreCallback}
          />
        </FullDialog>
          {!this.state.isLoading &&
        <EntitiesView
            //model={this.props.model}
          entities={entities}
          struct={this.state.entity}
          searchOptions={searchOptions}
          changeSearchOptions={this.changeSearchParameters}
          createCompany={this.createEntity}
          viewCompany={function(){}}
          editEntity={this.editCompany}
          editEntityCore={this.editCompanyCore}
          deleteEntity={this.deleteCompany}
        />
            }
      </React.Fragment>
    );
  }
}

export default Entities;