
import React, { useEffect, useState } from "react";
import  PropTypes from "prop-types";
import { withStyles } from '@material-ui/core/styles';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import Http from "entitytx/http/GenericService";

const styles = theme => ({
    root: {
        width: '100%',
        maxWidth: 360,
        borderWidth: "2px",
        borderColor: "grey",


    },
    inline: {
        display: 'inline',
    },
});



class EntityListItem extends  React.Component {

    /*
    Entity List Item.

    Returns an entity in event.value.target when onChange is set
     */
    state = {
      entity: this.props.entity,
      model: this.props.model,
      isLoading: true,
    }

    componentDidMount() {
        const  {entityId, modelName} = this.props;
        const {model, entity } = this.state;

        if(!model) {
          Http.struct(modelName)
            .then(model => {
              this.setState({model:model});
              return Http.get(model, entityId);
            })
            .then((entity) => {
              this.setState({entity: entity, isLoading: false});
            });
        }
        else if (!entity) {
          Http.group(model).then((entity) => {
            this.setState({entity: entity, isLoading: false});
          });
        }
      }


  render() {
    console.log("Rendering entity List item : ");

    const {
        key,
        onClick,
        children,
        entity,
        model,
      } = this.props;
    return (
       this.state.isLoading && "...Loading" ||
        <ListItem
            alignItems="flex-start"
            // TODO fix this
            onClick={e => {
                e.target.value = entity;
                e.target.id = key;
                onClick(e);
            }}
            key={entity._id}
            id={entity._id}
        >
            <ListItemAvatar>
                <Avatar
                    alt={entity.name}
                    src={
                        entity.img ?
                            entity.img :
                            "https://francophonie-en-fete.com/wp-content/uploads/2019/06/Louis-Jos%C3%A9-Houde-2019-copie-e1561481185366-600x614.jpg"
                    }
                />
            </ListItemAvatar>
            <ListItemText
                primary= { entity.primary || entity.name }
                secondary={
                    <React.Fragment>
                        <Typography
                            component="span"
                            variant="body2"
                            className={this.props.classes.inline}
                            color="textPrimary"
                        >
                            {entity.email || entity.secondary || children}
                        </Typography>
                        {entity.description}
                    </React.Fragment>
                }
            />
        </ListItem>
    )
    }
}

EntityListItem.defaultProps = {
  onClick: ()=>{}
};
EntityListItem.propTypes = {
  onClick: PropTypes.func,
  entity: PropTypes.object,
  model: PropTypes.object,
  modelName: PropTypes.string,
  entityId: PropTypes.string
};

export default withStyles(styles)(EntityListItem);
