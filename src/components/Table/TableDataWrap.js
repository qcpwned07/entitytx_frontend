import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import Table from "components/Table/CustomTable.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";
import Http from "entitytx/http/GenericService";
import EditButton from "../EditButton/EditButton";

const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0"
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF"
    }
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1"
    }
  }
};

const useStyles = makeStyles(styles);

export default function TableDataWrap(props) {
  const classes = useStyles();
  const [entities, setEntities] = useState();
  const [model, setModel] = useState(props.model);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    if(!props.model) {
      Http.struct(props.modelName)
        .then(model => {
          setModel(model);
          return Http.list(model);
        })
        .then((data) => {
          console.log(data)
          setEntities(data);
          setIsLoading(false);
        });
    }
    else if (!props.entities) {
      Http.list(props.model).then((entity) => {
        setEntities(entity);
        setIsLoading(false);
      });
    }
  }, []);


  // Define the rows

  console.log(entities);
  return (
    <div>
      {!isLoading && (
        <GridContainer>
          <GridItem xs={12} sm={12} md={12}>
            <Card>
              <CardHeader color="primary">
                <h4 className={classes.cardTitleWhite}>
                  {model.label}
                </h4>
                <p className={classes.cardCategoryWhite}>
                  {model.description}
                </p>
                <EditButton model={model}/>
              </CardHeader>
              <CardBody>
                <Table
                  tableHeaderColor="primary"
                  attr={model.attr}
                  entities={entities}
                />
              </CardBody>
            </Card>
          </GridItem>
        </GridContainer>
      )}
    </div>
  );
}

TableDataWrap.defaultProps ={
  modelName: "group"
}

TableDataWrap.propTypes = {
  model: PropTypes.object,
  entities: PropTypes.arrayOf(PropTypes.object),
  modelName: PropTypes.string,
};
