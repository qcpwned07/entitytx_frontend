import React from "react";
import PropTypes from "prop-types";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
// core components
import styles from "assets/jss/material-dashboard-react/components/tableStyle.js";
import EntityListItem from "components/List/EntityListItem";
import {capitalizeFirst} from "utils/Display";
//import { capitalize } from "@material-ui/core/utils";

const useStyles = makeStyles(styles);

export default function CustomTable(props) {
  const classes = useStyles();
  const { attr, entities, tableHeaderColor, onClick} = props;
  console.log("Table attrs")
  console.log(attr)
  return (
    <div className={classes.tableResponsive}>
      <Table className={classes.table}>
        {attr !== undefined ? (
          <TableHead className={classes[tableHeaderColor + "TableHeader"]}>
            <TableRow className={classes.tableHeadRow} key={"attr"}>
              {attr.map((attr, key) => {
                return (
                  <TableCell
                    className={classes.tableCell + " " + classes.tableHeadCell}
                    key={key}
                  >
                    {capitalizeFirst(attr.label)}
                  </TableCell>
                );
              })}
            </TableRow>
          </TableHead>
        ) : null}
        <TableBody>
          {entities.map((entity, key) => {
            console.log(entity)
            return (
              <TableRow key={entity._id} className={classes.tableBodyRow}>
                {entity.attr.map((attr, key) => {
                  return (
                    <TableCell className={classes.tableCell} key={attr.name}>
                      {attr.type === "entity" &&
                        // If an entity, return a listItem representing it
                        <EntityListItem
                          onClick={onClick}
                          entityId={attr.value}
                          modelName={attr.model}
                          key={attr.name}
                        />
                        // In case List return the length
                        || attr.type === "list" &&
                        `${attr.value.length}  ${capitalizeFirst(attr.label)}`
                      ||
                        attr.value
                      }
                    </TableCell>
                  );
                })}
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
    </div>
  );
}

CustomTable.defaultProps = {
  tableHeaderColor: "gray"
};

CustomTable.propTypes = {
  tableHeaderColor: PropTypes.oneOf([
    "warning",
    "primary",
    "danger",
    "success",
    "info",
    "rose",
    "gray"
  ]),
  attr: PropTypes.arrayOf(PropTypes.object),
  entities: PropTypes.arrayOf(PropTypes.object),
  onClick: PropTypes.func,
};
