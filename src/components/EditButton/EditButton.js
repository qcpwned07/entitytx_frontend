
import React from 'react';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import { Icon, IconButton } from "@material-ui/core";
import { Add, Delete, ThreeDRotation } from '@material-ui/icons';
import Card from "components/Card/Card";
import Modal from "@material-ui/core/Modal";
import EntityForm from "../Form/EntityForm";

const useStyles = makeStyles(theme => ({
  card: {
    zIndex: theme.zIndex.drawer + 1,
    backgroundColor: theme.backgroundColor,
    margin: '3%',
    maxWidth: '85%',
    padding:'2%',
    minHeight: '200px',
    maxHeight: '90%',
    overflow:'scroll',
  },
}));

export default function EditButton(props) {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const handleClose = () => {
    setOpen(false);
  };
  const handleToggle = () => {
    setOpen(!open);
  };

  return (
    <div>
      <IconButton
        variant="outlined"
        color="white"
        backgroundColor="white"
        aria-label={"edit"}
        onClick={handleToggle}>
          <Add />
        </IconButton>

      <Modal
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        open={open}
        onClose={handleClose}
      >
        <Card className={classes.card}>
          <EntityForm />

        </Card>
      </Modal>
    </div>
  );
}
