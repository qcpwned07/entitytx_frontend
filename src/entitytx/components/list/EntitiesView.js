import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import defaultStrings from '../../../i18n/en.json';
import ExtendedFab from '../../../components/Styled/ExtendedFab';
import StyledTab from '../../../components/Styled/StyledTab';
import StyledTabs from '../../../components/Styled/StyledTabs';
import StringInput from '../../../components/Input/StringInput';
import Scene from '../../../components/Scene/Scene';
import SceneContent from '../../../components/Scene/SceneContent';
import SceneHeader from '../../../components/Scene/SceneHeader';
import SceneTitle from '../../../components/Scene/SceneTitle';
import NeuligentGrid from '../../../components/utils/NeuligentGrid.js';
import Button from '@material-ui/core/Button';

import crossIcon from '../../../images/cross.svg';
import MenuItem from "@material-ui/core/esm/MenuItem/MenuItem";
import Menu from "@material-ui/core/es/Menu/Menu";
import ButtonMenu from "../../../components/utils/Buttons/ButtonMenu";
import {capitalizeFirst, hasChildren} from "../../../utils/Display";


class EntitiesView extends React.Component {
  state = {
      currentTab: 0,
      selectedEntity: {},
      anchorEl: 0,
      addAnchorEl: 0,
  };

  componentDidMount(){
      // TODO GET RELEVANT DATA
  }


  render() {
    const { entities, struct, searchOptions, changeSearchOptions, viewCompany, createCompany, editCompany, editCompanyCore, deleteCompany } = this.props;
    const { currentTab } = this.state;

    // TODO MAP COLUMNS BASED ON PREVIEW COLUMNS
    const columns = struct.preview.map(attr=> {
        return {
            label: capitalizeFirst(attr.label),
            dataKey: attr.primary ? 'searchLabel' : attr.name ? attr.name : "no name",
            key: attr.primary ? 'searchLabel' : attr.name ? attr.name : "no name",
            width: 150,
        }
    });
    columns.push({
            label: "Actions",//<FormatedString {...defaultStrings.singleWords.action}/>,
            dataKey: 'action',
            key: 'action',
            width: 80,
    })

    let rows = [];
    // Add a row for each entity in list
    if (entities)
      for (const entity of entities) {
          var name = entity.values && entity.values.length !== 0  &&
              entity.values.find(value => value.name === 'name');

          // If no attr name, select the entity name (temporary)
          if(name === false){
              name = entity.name;
          } else if (typeof name === "undefined")
              name = entity.name;
          else if(name.value)
              name = name.value;
          if (!name)
              name = "no name";

          // Create the row
          var row = {};
          console.log("Struct preview: \n");
          console.log(struct);
          struct.preview.map(attr => {
              row.width = 100;
              row[attr.primary ? 'searchLabel' : attr.name] =
                  entity.values.find(value => value.name === attr.name) ?
                    entity.values.find(value => value.name === attr.name).value
                      :
                    "No name"  ;

              row.key = attr.name;
              //console.log(row);
              //console.log(entity.values.find(value => value.name === attr.name));

          });
          // Add actions
          row.action =(
              <ButtonMenu>
                  <MenuItem onClick={() => this.props.editEntity(entity)}>
                      <FormattedMessage {...defaultStrings.singleWords.edit} />
                  </MenuItem>
                  <MenuItem onClick={() => this.props.editEntityCore(entity)}>
                      <FormattedMessage {...defaultStrings.singleWords.manage} />
                  </MenuItem>
                  <MenuItem onClick={() => this.props.deleteEntity(entity)}>
                      <FormattedMessage {...defaultStrings.singleWords.delete} />
                  </MenuItem>
              </ButtonMenu>
          );
          row.onRowClick= ()=>{};
          // Add new row to list
          rows.push(row);

      }

    return (
      <Scene>

        <SceneTitle>
            {struct.description? capitalizeFirst(struct.description) : capitalizeFirst(struct.name)}
        </SceneTitle>

         <SceneHeader justify="space-between">
              <StyledTabs value={currentTab} onChange={(event, currentTab) => this.setState({ currentTab })}>
                  {this.props.struct.tabs.map(tab =>
                      <StyledTab label={ capitalizeFirst(tab.label)} />)
                  }
              </StyledTabs>
          <ExtendedFab onClick={createCompany}>
              {"Ajouter un " + this.props.struct.label}
          </ExtendedFab>
        </SceneHeader>

          <SceneContent>

            <NeuligentGrid
              columns={columns}
              rows={rows}
              searchMessage={defaultStrings.companyProfilePage.searchInputLabel}
              onClickEdit={()=>{} /*this.props.editEntityCore*/}
              onRowClick={()=>{} /* this.props.editEntityCore*/}
              deleteUser={this.props.deleteEntity}
            />

          </SceneContent>
      </Scene>
    );
  }
}

EntitiesView.propTypes = {
  entities: PropTypes.arrayOf(PropTypes.object),
  struct: PropTypes.arrayOf(PropTypes.object),
  searchOptions: PropTypes.object.isRequired,
  changeSearchOptions: PropTypes.func.isRequired,
  createCompany: PropTypes.func.isRequired,
  editEntity: PropTypes.func.isRequired,
  editEntityCore: PropTypes.func.isRequired,
  deleteEntity: PropTypes.func.isRequired,
};

export default EntitiesView;
