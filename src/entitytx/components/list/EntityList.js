
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import EntityListItem from "./EntityListItem";
import Http from "../../../http/GenericService_special";

const styles = theme => ({
    root: {
        width: '100%',
        maxWidth: 760,
        position: 'relative',
        overflow: 'auto',
        maxHeight: 400,
    },
    inline: {
        display: 'inline',
    },
});

class EntityList extends  React.Component {

    constructor(props) {
        super(props);
        this.state = {
            selectedIndex: 0,
            isLoading: true,
            entities: this.props.entities,
            model: this.props.model
        };

        this.handleListItemClick = this.handleListItemClick.bind(this);
    }

    componentDidMount(){

        // If no list was provided, load one
        if (!this.props.list)
            Http.list(this.props.model).then( response => {
                this.setState({entities: response, isLoading: false});
            })
        else
            this.setState({isLoading:false});
    }

    /*
    To be executed on onclick.
       Change selected item and execute props.onClick
       @param event;
       @param index: new index
     */
    handleListItemClick = (event, index) => {
        this.setState({selectedIndex:index});
        if(this.props.onClick)
            this.props.onClick();
    };


    render() {
        var {classes, attr, onClick, avatarSize} = this.props;
        var indexes = 1;

        return (
            <List className={classes.root}>
                {this.props.list.map(entity =>
                    <EntityListItem
                        attr={attr}
                        onClick={onClick}
                        entity={entity}
                        avatarSize={avatarSize}
                    />
                )}

                <Divider variant="inset" component="li"/>

            </List>
        );
    }
};


export default withStyles(styles)(EntityList);