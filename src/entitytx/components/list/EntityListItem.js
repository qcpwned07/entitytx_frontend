
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import PropTypes from 'prop-types';

const styles = theme => ({
    root: {
        width: '100%',
        maxWidth: 360,
        borderWidth: "2px",
        borderColor: "grey",


    },
    inline: {
        display: 'inline',
    },
});



class EntityList extends  React.Component {

    /*
    Entity list Item.

    Returns an entity in event.value.target when onChange is set
     */
    render() {

        const {attr,  entity } = this.props;
    return (
        <ListItem
            alignItems="flex-start"
            // TODO fix this
            onClick={e => {
                e.target.value = entity;
                e.target.name = attr.name;
                e.target.id = attr.name;
                this.props.onClick(e);
            }}
            key={entity._id}
            id={attr.name}
        >
            <ListItemAvatar >
                <Avatar
                    alt={entity.name}
                    src={
                        entity.img ?
                            entity.img :
                            "https://francophonie-en-fete.com/wp-content/uploads/2019/06/Louis-Jos%C3%A9-Houde-2019-copie-e1561481185366-600x614.jpg"
                    }
                />
            </ListItemAvatar>
            <ListItemText
                primary= {//entity.values.find(value =>  value.primary)
                    entity.firstName + " " + entity.lastName
                     }
                secondary={
                    <React.Fragment>
                        <Typography
                            component="span"
                            variant="body2"
                            className={this.props.classes.inline}
                            color="textPrimary"
                        >
                            {entity.email}
                        </Typography>
                        {entity.description}
                    </React.Fragment>
                }
            />
        </ListItem>
    )
    }
}

EntityList.propTypes ={
    entity: PropTypes.object.isRequired,
    avatarSize: PropTypes.number,
}

export default withStyles(styles)(EntityList);
