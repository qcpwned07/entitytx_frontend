import React from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import { connect } from "react-redux";
import { bindActionCreators } from 'redux';
import { FormattedMessage } from 'react-intl';
import { localUpdateUser, setUser } from '../../../store/actions/user.actions';
import defaultStrings from '../../../i18n/en.json';
//import validator from 'validator';
import { displayError } from '../../../components/utils/SnackbarMessage.js';
import GenericFormView from './GenericFormView';
import Http from '../../../http/GenericService_special';

const mapStateToProps = state => {
  return { user: state.userState.user };
};

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ localUpdateUser, setUser }, dispatch);
}

class EntityForm extends React.Component {
  state = {
    isLoading: true,
    entity: null,
    model: '',
    values:{},
    struct: {},
    user: null,
    passwordData: { oldPassword: '', newPassword: '', confirmNewPassword: '' },

  };
      /*  Dynamic form generator
        @params entity: an object of the type we want to edit
     */
    constructor(props) {
        super(props);
        if(this.props.id){
            this.state.id = this.props.id;
        }
        this.state.model = this.props.model;
        this.state.struct = this.props.struct;

        // Get the structure of given entity
        Http.struct(this.state.model).then(response => {
          if(this._isMounted) {
              this.setState(Object.assign({}, this.state, {struct: response}));

              // Generating a base entity if not provided
              var values = {};
              // Take the attributes of the struct, and create object fiel
              // for each of them, and initialize.
              response.attr.forEach( attr => {
                  if(values[attr.name] === undefined){
                      // In case it is a list, change default type
                      if(attr.type === 'list')
                          values[attr.name] = [];
                      //All other types
                      else
                          values[attr.name] = '';
                  }
              })
              this.setState(Object.assign({}, this.state, { values: values}));
              this.setState(Object.assign({}, this.state, {isLoading: false}));

          }
      });
        // If an id was given, retrieve it
        if(this.props.id) {
            Http.get(this.state.model, this.state.id).then(response => {
                if (this._isMounted) {
                    this.setState(Object.assign({}, this.state, {entity: response}));
                }
              var values = this.state.values;
              response.values.forEach( value => {
                      values[value.name] = {attr: value.attr, value: value.value};
              })
              this.setState(Object.assign({}, this.state, { values: values}));
            });
        }
        // Dynamically bind the different handleChange functions
        /*
        for (const key in Object.keys(this.props.entity)) {
            this['handleChange'+key] = this['handleChange'+key].bind(this);
        }
        */
        this.handleChange = this.handleChange.bind(this);
        this.changeEntity = this.changeEntity.bind(this);
        this.changeEntityCore = this.changeEntityCore.bind(this);

    }

    //TODO:  Works with babel and ES6, a voir si marche ici
    handleChange(event) {
        // Find the id we are looking for
        var name = this.state.struct.attr.find(attr => attr.name === event.target.id)._id;

        var values = this.state.values;
        // Create the field with the name of the attr (event.target.id)
        values[event.target.id] = {
            attr: this.state.struct.attr.find(attr => attr.name === event.target.id)._id,
            value: event.target.value
        };
        this.setState(Object.assign({}, this.state, {values: values}));
    }
    /*
    handleChange_b : function (event) {
        this.setState({[event.target.id]: event.target.value});
    }
    */

    /*
    When Loading compoonent:
      - Retrieve the data from the current entity if exists
     */
  componentDidMount() {
    //TODO RETRIEVE REQUIRED DATA
      this._isMounted= true;

  }

    componentWillUnmount() {
        this._isMounted = false;
    }

  // TODO CHANGE FOR REAL CALL
  retrieveEntity = (id) => {
    var self = this;

    Http.get(this.props.model, id).then(response => {
      self.setState({ entity: response});
    });
  };

  processForm = () => {

      // TODO VALIDATE ALL REQUIRED FIELD ARE THERE

      // Create the entity
      var entity = {
          name: this.props.model,
          values: this.state.values,
      };
      // Put the values into an array
      entity.values = Object.keys(entity.values).map(function(key) {
          return {
              attr: entity.values[key]._id,
              name: key,
              value: entity.values[key].value};
      });
      // Post said array

      // TODO CHANGE POST FOR PUT
        Http.post(this.props.model, entity).then(response => {
          // Put the created entity back in the state
            this.setState({ entity: response });
            this.setState({successMessage: true});

            // If a callback was provided, call it
            if (typeof this.props.callback !== 'undefined')
                this.props.callback();

      });
  };

  processPasswordForm = () => {
    const { admin } = this.props;
    const { passwordData, user } = this.state;
    const isAdmin = (typeof admin !== 'undefined') ? admin : false;
    const self = this;

    if (passwordData.newPassword === '') {
      return displayError(<FormattedMessage {...defaultStrings.userForm.newPasswordRequiredWarning} />);
    }

    if (passwordData.confirmNewPassword !== passwordData.newPassword) {
      return displayError(<FormattedMessage {...defaultStrings.userForm.passwordMismatchWarning} />);
    }

    if (!isAdmin && passwordData.oldPassword === '') {
      return displayError(<FormattedMessage {...defaultStrings.userForm.oldPasswordRequiredWarning} />);
    }

    const url = '/api/auth/password' + (isAdmin ? '/' + user.id : '');

    axios.put(url, passwordData, { successMessage: true }).then(() => {
      self.setState({
        passwordData: { oldPassword: '', newPassword: '', confirmNewPassword: '' },
      })
    });
  };

  changeEntity = (event) => {
      // Map the event to the right field
      var { name, value } = event.target;
      // If input is a checkbox
      if (event.target.hasOwnProperty('checked')) {
          value = false;
          if (event.target.checked) {
              value = true;
          }
      }
      // Save changes
      var entity = { ...this.state.entity };
      entity[name] = value;
      this.setState({ entity });
    };


  changeEntityCore = event => {
    const { name, value } = event.target;
    var entity = { ...this.state.entity };
    entity[name] = value;
    this.setState({ entity });
  };


  render() {
    return (
        <div>
        {!this.state.isLoading &&//this._isMounted &&
      <GenericFormView
        entity={this.state.entity}
        values={this.state.values}
        onChange={this.handleChange /*todo change core*/}
        changeEntityCore={this.handleChange}
        onSubmit={this.processForm}
        model={this.state.model}
        struct={this.state.struct}
        //admin={this.props.admin}
      />}
            {this.state.isLoading &&
                <h3>Loading...</h3>
            }

        </div>
    );
  }
}

EntityForm.contextTypes = {
  router: PropTypes.object.isRequired,
  model: PropTypes.string.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(EntityForm);
