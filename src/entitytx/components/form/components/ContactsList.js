
import React from 'react';
import SearchIcon from '@material-ui/icons/Search';
import { FormattedMessage } from 'react-intl';
import TextField from '@material-ui/core/TextField';
import { withStyles } from '@material-ui/core';
import List from '@material-ui/core/List';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import lightBaseTheme from 'material-ui/styles/baseThemes/lightBaseTheme';
import defaultStrings from '../../../../i18n/en.json';

const styles = theme => ({
    searchBar: {
        borderRadius: '25px',
        width: "320px",
        height: "60px",
    },
    dataTable: {
        display: "flex",
        FlexDirection: "column",
        justifyContent: "left",

    },
    headerGrid: {
        marginBottom: "25px",
        display: "flex",
        FlexDirection: "row",
    },
    greendot: {
        height: "10px",
        width: "10px",
        borderRadius: "25px",
        background: "green"
    },
    reddot: {
        height: "10px",
        width: "10px",
        borderRadius: "25px",
        background: "red"
    },
    dataCell: {
        display: "flex",
        FlexDirection: "row",
        borderRadius: "440px",
        boxShadow: "2px 2px 1px 1px",
        width: "320px",
        height: "80px",
        margin: "10px",
    },
    dataCellTitle: {
        wordWrap: "break-word",
        width: "200px",
        fontWeight: "bold",
        marginBottom: "4px",
        marginLeft: "8px",
    },
});


class ContactsList extends React.Component {
    state = {
        model: this.props.model,    // String of the model name to pull list
        preview: this.props.preview,// List of preview values
        search: '',
    };



    renderRows(data, classes) {
        return data.filter(row => row.name.includes(this.state.search)).map(el => {
            return <MuiThemeProvider muiTheme={getMuiTheme(lightBaseTheme)}>
                <div onClick={(e) => this.props.onClick(el)} className={classes.dataCell} >
                    {el.image}
                    <p className={classes.dataCellTitle}>{el.name}</p>
                    <div style={{ marginTop: "20px", marginRight: "15px" }}>
                        {el.status == "seen"? <div className={classes.greendot}></div> : <div className={classes.reddot}></div>}
                        <p style={{ marginTop: "-13px", marginLeft: "11px" }}>{el.time}</p>
                    </div>
                </div>
            </MuiThemeProvider>
        })
    }

    render() {
        const { classes, rowsPage, onClick, } = this.props;
        const { } = this.state;

        return (
            <div>
                <div>
                    <div className={classes.headerGrid}>
                        <FormattedMessage {...defaultStrings.block.searchcontactorgroup}>
                            {placeholder =>
                                <TextField
                                    name="url"
                                    placeholder={placeholder}
                                    variant="outlined"
                                    onChange={this.updateSearch}
                                    InputProps={{
                                        className: classes.searchBar,
                                        endAdornment: <SearchIcon />,
                                    }}
                                />
                            }
                        </FormattedMessage>
                    </div>
                </div>
                <div
                    className={classes.dataTable}
                    style={{ height: "600px", display: 'block', overflowY: 'auto', position: 'relative', width:"100%" }}>
                    <List style={{ columns: "1 auto", }}>
                        {this.renderRows(rowsPage, classes)}
                    </List>
                </div>
            </div>
        );
    }
}

export default withStyles(styles)(ContactsList);
