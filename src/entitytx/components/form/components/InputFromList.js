
import React from 'react';
import PropTypes from 'prop-types';
import defaultStrings from '../../../../i18n/en.json';
import EntityList from "../../list/EntityList";
import axios from 'axios';
import {TextField} from "material-ui";
import { withStyles } from '@material-ui/core/styles';
import SearchIcon from '@material-ui/icons/Search';
import Http from "../../../../http/GenericService_special";
import StringInput from "../../../../components/Input/StringInput";
import {capitalizeFirst, hasChildren} from "../../../../utils/Display";


const styles = theme => ({
    searchBar: {
        borderRadius: '25px',
        width: "320px",
        height: "60px",
    },
})



class InputFromList extends React.Component {
    /*
    Class generating an <option> input field for a form,
    @param model: (String) The model we are pulling the options from
    @param list: (Entity) e reference of the values to be changed when adding, removing
    @param values: (Value) The object value array to be displayed when adding, removing
    @param onAdd: (Func) The function to be called when an item is added to the list
    @param onDelete: (Func) The function to be called when an item is removed to the list
    @param attr: (Attr) The attribute referenced by this field
     */

    constructor(props) {
        super(props);
        this.state = {
            model: this.props.model,
            values: this.props.values,
            list: this.props.list,
            onAdd: this.props.onAdd,
            onDelete: this.props.onDelete,
            isLoading: true,
            isEdit: true,
        }
    }

    componentDidMount() {
        // If it is user, call the classical search
        if(this.props.model === 'users' || this.props.model === 'user')
            axios.post('/api/user/search', ).then(response => {
                this.setState({ list: response.data.users, isLoading: false });
            });
        // For other models, call the generic http service
        else {

            Http.list(this.props.model).then(response =>
                this.setState({list: response, isLoading: false})
            );
        }
    }


  updateSearch = search => {
    let dataTailLimit = this.props.rows.filter(row => row.searchLabel.includes(search.target.value)).length;
    let newMaxPage = Math.ceil(dataTailLimit/this.state.nbToShow);
    this.setState({ search: search.target.value,
      page: this.state.page > newMaxPage ?  newMaxPage : (this.state.page === 0 && newMaxPage > 0 ?  1 : this.state.page) });
  };

    addEntity = (event, entity) => {

    }

    render() {
        const attr = this.props.attr

        if (this.state.isLoading)
            return (<div>Loading...</div>);
        else
            return (<div>
            {/*placeholder =>
              <TextField
                name="url"
                placeholder={placeholder}
                variant="outlined"
                onChange={this.updateSearch}
                InputProps={{
                  className: this.props.classes.searchBar,
                  endAdornment: <SearchIcon />,
                }}
              />
           */ }
            <h3>{capitalizeFirst(attr.label)}</h3>
            <h4>{capitalizeFirst(attr.description)}</h4>
            <StringInput
                    id={attr.name}
                    key={attr._id}
                    value={this.state.search}
                    placeholder={attr.label}
                    type="text"
                    onChange={()=>{}}
                    disabled={false}
                />
                {this.state.values}
                {this.props.values.map(value => {
                    return (<p>{value}</p>);
                })
                }
            <EntityList
                attr={this.props.attr}
                list={this.state.list}
                model={this.props.model}
                onClick={ this.props.onChange}

            />
        </div>)
    }
}



InputFromList.propTypes = {
    model: PropTypes.string.isRequired,
    values: PropTypes.array.isRequired,
    list: PropTypes.array,
    onAdd: PropTypes.func,
    onRemove: PropTypes.func,
};

export default withStyles(styles)(InputFromList);