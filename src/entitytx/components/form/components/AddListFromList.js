
import React from 'react';
import PropTypes from 'prop-types';
import defaultStrings from '../../../../i18n/en.json';
import EntityList from "../../list/EntityList";
import axios from 'axios';
import {TextField} from "material-ui";
import { withStyles } from '@material-ui/core/styles';
import SearchIcon from '@material-ui/icons/Search';
import Http from "../../../../http/GenericService_special";
import StringInput from "../../../../components/Input/StringInput";
import {capitalizeFirst, hasChildren} from "../../../../utils/Display";
import {bindActionCreators} from "redux";
import {deleteAccount, getAccounts} from "../../../../store/actions/account.actions";
import {connect} from "react-redux";
import NeuligentButton from "../../../../components/utils/Buttons/NeuligentButton";
import {FormattedMessage} from "react-intl";
import DialogButton from "../../../../components/utils/Buttons/DialogButton";
import {Grid} from "@material-ui/core";


const styles = theme => ({
    searchBar: {
        borderRadius: '25px',
        width: "320px",
        height: "60px",
    },
})



class AddList extends React.Component {
    /*
    Class generating an <option> input field for a form,
    @param model: (String) The model we are pulling the options from
    @param list: (Entity) e reference of the values to be changed when adding, removing
    @param values: (Value) The object value array to be displayed when adding, removing
    @param onAdd: (Func) The function to be called when an item is added to the list
    @param onDelete: (Func) The function to be called when an item is removed to the list
    @param attr: (Attr) The attribute referenced by this field
     */

    constructor(props) {
        super(props);
        this.state = {
            model: this.props.model,
            values: this.props.value || [],
            list: this.props.list,
            onAdd: this.props.onAdd,
            onDelete: this.props.onDelete,
            isLoading: !!this.props.users ,
            edit: true,
        }
    }

    componentDidMount() {

        if(!this.props.users) {
            this.props.getAccounts();
        } else {
            this.setState({isLoading: false});
        }
            /*
            if(this.props.model === 'users' || this.props.model === 'user')
                axios.post('/api/user/search', ).then(response => {
                    this.setState({ list: response.data.users, isLoading: false });
                });
            else {
                console.log(this.props.model);
                Http.list(this.props.model).then(response =>
                    this.setState({list: response, isLoading: false})
                );
            }
            */
    }


  updateSearch = search => {
    let dataTailLimit = this.props.rows.filter(row => row.searchLabel.includes(search.target.value)).length;

    let newMaxPage = Math.ceil(dataTailLimit/this.state.nbToShow);
    this.setState({ search: search.target.value,
      page: this.state.page > newMaxPage ?  newMaxPage : (this.state.page === 0 && newMaxPage > 0 ?  1 : this.state.page) });
  };

    addEntity = (event, entity) => {

    }

    onChange = e => {
        // If value is defined (necessary for click outside items)
        if(e.target.value)
            if (!this.props.returnValue){
                let values = this.props.value || [];
                values.push(e.target.value);
                e.target.value = values;
                this.props.onChange(e);
            } else {
                let values = this.state.values.concat(e.target.value) || [];
                //values.push(e.target.value);
                this.setState({values: values});
                // TODO Refactor to enable the returnValue option(returns instead of change directly)
                e.target.value = values;
                this.props.onChange(e);
            }
    }

    render() {
        const {attr, model, edit, value, avatarSize, users} = this.props;
        const {values} = this.state;

        if (this.state.isLoading)
            return (<div>Loading...</div>);
        else
            return (
                <Grid container item spacing={2} xs={12} >

                {/*<h3>{capitalizeFirst(attr.label)}</h3>
                <h4>{attr.description && capitalizeFirst(attr.description)}</h4>*/}

               <Grid item xs={12} md={12}>
                    <EntityList
                        attr={attr}
                        list={values ||[]}
                        model={model}
                        onClick={()=>{}}
                    />
                </Grid>
                {   // If editable, show add
                    this.props.edit &&
                <Grid item xs={12} md={12}>
                    <StringInput
                            id={attr.name}
                            key={attr._id}
                            value={this.state.search}
                            placeholder={attr.label}
                            type="text"
                            onChange={()=>{}}
                            disabled={false}
                        />
                    <EntityList
                        attr={attr}
                        // TODO Remove slice, find way to not render 1268 users (lag ++)
                        list={users.slice(0, 50)}
                        model={model}
                        onClick={this.onChange}
                        avatarSize={avatarSize}

                    />
                </Grid>
                    }
        </Grid>)
    }
}



AddList.propTypes = {
    model: PropTypes.string.isRequired,
    attr: PropTypes.object.isRequired,
    //list: PropTypes.array.isRequired,
    value: PropTypes.array.isRequired,
    avatarSize: PropTypes.number,
    returnValue: PropTypes.bool,
    //onAdd: PropTypes.func.isRequired,
    //onRemove: PropTypes.func.isRequired,
};

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ getAccounts: getAccounts, deleteAccount: deleteAccount }, dispatch);
}

const mapStateToProps = state => {
    return { users: state.accountState.users };
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(AddList));
