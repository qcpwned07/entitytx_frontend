import React from 'react';
import { FormattedMessage } from 'react-intl';
import defaultStrings from '../../../i18n/en.json';
import Lang from '../../../utils/Lang';
import Scene from '../../../components/Scene/Scene';
import SceneActions from '../../../components/Scene/SceneActions';
import SceneHeader from '../../../components/Scene/SceneHeader';
import ExtendedFab from '../../../components/Styled/ExtendedFab';
import StyledTab from '../../../components/Styled/StyledTab';
import StyledTabs from '../../../components/Styled/StyledTabs';
import UserNotes from '../../../components/utils/Notes/Notes';
import AccountPanel from './components/components/AccountPanel/AccountPanel.js';
import PasswordPanel from './components/components/PasswordPanel.js';
import GroupForm from './components/components/GroupForm.js';

//Added by Matth
import PropTypes from 'prop-types';
import {TextareaInput} from "../../../components/Input/TextareaInput";
import StringInput from "../../../components/Input/StringInput";
import { withStyles } from '@material-ui/core/styles';
import {capitalizeFirst, hasChildren} from "../../../utils/Display";
import SearchIcon from '@material-ui/icons/Search';
import testIcon from '../../../images/face_test.png';
import InputFromList from "./components/InputFromList";
import Grid from "@material-ui/core/es/Grid/Grid";
import InputListFromList from "./components/ListInput";
//import ContactsList from "./components/ContactsList";

const styles = {
     subtitles: {
        fontSize: "25px",
        color: "#11579C",
        borderBottom: "3px solid #11579C"
    },
    root: {
        flexGrow: 1,
    }
}



class GenericFormView extends React.Component {
  state = {
    currentTab: 0,
  };


  componentDidMount(){

    // TODO retrieve object model
      // TOdo : Tabs
      // Todo : Sections
      // Todo :
  }

  render() {
    const {
        entity,
        onChange,
        changeEntityCore,
        processForm,
        model,
        classes,
        values,
        struct,
        admin } = this.props;
    const { currentTab } = this.state;

    if (!struct) {
      return null;
    }

    // Find the title in the values
    var title;
    var lists

    // Extract informations from props
    if(struct.attr && this.props.values) {

        // Find the name of the primary attribute
        var attrName = struct.attr.find(attr => attr.primary);
        // Find this value
        title = values[attrName]

    }


    return (
        <Scene noPaper>

            <div className={styles.root}>

            <Grid container spacing={10}>

            <Grid item xs={12} lg={9}>
                <div style={styles.subtitles}>
                    {capitalizeFirst(struct.label)}
                </div>
                <h3>{title}</h3>
            </Grid>



            <Grid container spacing={3} item sm={12} md={6} xl={6}>
                {struct.attr.map(attr => {
                    // If it is a section
                    // TODO put <Typography instead
                    if (attr.type === "subsection" || attr.type === "section")
                        return (<Grid item >
                        <h2>{capitalizeFirst(attr.label)}</h2>
                            </Grid>);

                    // If it is a subsection

                    // In case it is a string
                    else if (attr.type === "string")
                        return (
                            <Grid item sm={12} md={12} lg={6} >
                            <div>
                                    <h3>{capitalizeFirst(attr.label)}</h3>
                                    <h4>{capitalizeFirst(attr.description)}</h4>
                                    <StringInput
                                        id={attr.name}
                                        key={attr._id}
                                        value={this.props.values[attr.name].value}
                                        type="text"name
                                        onChange={this.props.onChange}
                                    />
                            </div>
                            </Grid>
                    );
                    // In case it is a list of entity

                    }
                )}
            </Grid>

            <Grid container spacing={12} item xs={12} md={6} lg={5} xl={4}>
                {struct.attr.map(attr => {
                // Put the lists on the side
                    if (attr.type === "list")
                        return (
                            <div className={styles.root}>
                            <InputListFromList
                                attr={attr}
                                model={attr.model}
                                onChange={e => {
                                    e.target.value = this.props.value.push(e.target.value);
                                    this.props.onChange(e)
                                }}
                                value={this.props.values[attr.name]}
                            />
                            </div>
                        );
                    else if (attr.type === "entity")
                        return (
                            <Grid item sm={12} md={12} lg={12} xl={6} >
                                <div>
                                    <InputFromList
                                        attr={attr}
                                        model={attr.model}
                                        value={this.props.values[attr.name]}
                                        onChange={this.props.onChange}
                                    />
                                </div>
                            </Grid>
                        );
                        else return <div></div>
                })}
            </Grid>

            </Grid>
            <ExtendedFab onClick={this.props.onSubmit} >Soumettre le {this.props.struct.label}</ExtendedFab>
            </div>
        </Scene>

    )
  }
};

GenericFormView.propTypes = {
    struct: PropTypes.object.isRequired,
    onChange: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired,
};

export default GenericFormView;
